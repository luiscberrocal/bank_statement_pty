from unittest import TestCase
from bank_statement.utils import split_path

__author__ = 'luiscberrocal'


class TestUtils(TestCase):

    def test_split_path(self):
        results = {'extension': 'html', 'path': '/my_path', 'basename': 'test', 'filename': 'test.html'}
        filename = '/my_path/test.html'
        filename_data =  split_path(filename)
        print(filename_data)
        self.assertEqual(results, filename_data)

    def test_split_path_mutli_dot(self):
        results = {'extension': 'html', 'path': '/my_path', 'basename': 'test.old', 'filename': 'test.old.html'}
        filename = '/my_path/test.old.html'
        filename_data =  split_path(filename)
        print(filename_data)
        self.assertEqual(results, filename_data)
