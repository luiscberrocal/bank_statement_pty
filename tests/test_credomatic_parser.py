from bank_statement.parsers.credomatic import convert_date
from datetime import date
__author__ = 'luiscberrocal'

import unittest


class CredomaticParserTestCase(unittest.TestCase):
    def test_convert_date(self):
        expected = date(2015,1,4)
        test_date = convert_date('ENE/04', '2015')
        self.assertEqual(expected, test_date)




if __name__ == '__main__':
    unittest.main()
