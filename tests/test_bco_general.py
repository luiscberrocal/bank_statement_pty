import os
from unittest import TestCase
from bank_statement.parsers.bco_general import bank_data, _get_account_type, _convert_to_date
from bank_statement.settings.test import FOLDER_DATA
from datetime import date
__author__ = 'luiscberrocal'



class TestBcoGeneral(TestCase):

    def test__convert_to_date(self):
        rdate = date(2015,1,3)
        tdate = _convert_to_date('03/Ene/2015')
        self.assertEqual(rdate, tdate)

    def test__get_account_type(self):
        account_number = '03-77-01-077777-0'
        account_type = _get_account_type(account_number)
        self.assertEqual('Checking account', account_type)
        
    def test_bank_data(self):
        source_filename = os.path.join(FOLDER_DATA, 'cuenta_corriente_201401_files', 'estados_cta.html')
        result = {'account_number': '03-77-01-077777-0',
                  'account_name': 'JAMES TIBERIUS KIRK SPOCK',
                  'source_filename': source_filename,
                  'account_type': 'Checking account',
                  'account_date': None,}

        filename = os.path.join(FOLDER_DATA, 'cuenta_corriente_201401.html')
        data = bank_data(filename)
        self.assertEqual(result, data)

    def test_bank_data_visa(self):
        basename = 'visa_clasica_201505'
        source_filename = os.path.join(FOLDER_DATA, '%s_files' % basename, 'visa_mov.html')
        result = {'account_number': '4511-11XX-XXXX-1111',
                  'account_name': 'JAMES TIBERIUS KIRK',
                  'source_filename': source_filename,
                  'account_type': 'VISA account',
                  'account_date': date(2015,5,17)}

        filename = os.path.join(FOLDER_DATA, '%s.html' % basename)
        data = bank_data(filename)
        print(data)
        self.assertEqual(result, data)

