import os
from unittest import TestCase
from bank_statement.files.credomatic_manager import list_new_files
from bank_statement.settings.test import FOLDER_DATA

__author__ = 'luiscberrocal'


class TestList_new_files(TestCase):

    def test_list_new_files(self):
        regexp = r'credomatic_sample_file_?\d?\.html'
        new_files = list_new_files(regexp, FOLDER_DATA)
        self.assertEqual(len(new_files), 3)
