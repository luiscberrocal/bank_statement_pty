import os
from unittest import TestCase
from bank_statement.parsers.bco_general import parse_html_account, parse_general_visa_html
from bank_statement.settings.test import FOLDER_DATA, OUTPUT_FOLDER
from _datetime import datetime
from bank_statement.writers.excel import write_excel

__author__ = 'luiscberrocal'


class TestExcel(TestCase):

    def test_write_excel(self):
        basename = 'cuenta_corriente_201401'
        source_filename = os.path.join(FOLDER_DATA, '%s_files' % basename, 'estados_cta.html')
        transactions = parse_html_account(source_filename, 'Cuenta corriente')
        excel_filename = os.path.join(OUTPUT_FOLDER,
                                      'test_write_excel_%s_%s.xlsx' % (basename,
                                                                       datetime.now().strftime('%Y%m%d_%H%M')))

        write_excel(transactions, excel_filename)
        self.assertTrue(os.path.exists(excel_filename))

    def test_write_excel_visa(self):
        basename = 'visa_clasica_201505'
        source_filename = os.path.join(FOLDER_DATA, '%s_files' % basename, 'visa_mov.html')
        transactions = parse_general_visa_html(source_filename, 'VISA')
        excel_filename = os.path.join(OUTPUT_FOLDER,
                                      'test_write_excel_%s_%s.xlsx' % (basename,
                                                                       datetime.now().strftime('%Y%m%d_%H%M')))

        write_excel(transactions, excel_filename)
        self.assertTrue(os.path.exists(excel_filename))


