import json
import re
from appdirs import user_config_dir
from bank_statement.settings.config import write_default_config
import collections
__author__ = 'luiscberrocal'
import logging
import configparser
from os.path import dirname, abspath, exists, join, basename
from os import getuid

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

BANK_STATEMENT_ROOT = dirname(dirname(abspath(__file__)))
SITE_NAME = basename(BANK_STATEMENT_ROOT)
BANK_STATEMENT_CONFIGURATION_DIR = user_config_dir(SITE_NAME, 'viber')
BANK_STATEMENT_CONFIGURATION_FILE = join(BANK_STATEMENT_CONFIGURATION_DIR, 'default.json')


configuration_filename = join(BANK_STATEMENT_ROOT, 'settings', 'default.json')
if not exists(configuration_filename):
    write_default_config(configuration_filename)

with open(configuration_filename, 'r') as json_file:
    config = json.load(json_file)

COLUMN_HEADER = ['Transaction Date', 'Process Date', 'Bank Id', 'Description', 'Amount', 'Type', 'Source', 'Classification']

FOLDER_DATA = config['user_data']['data_path'] #configuration.get('UserData', 'path') #'/Users/luiscberrocal/Downloads/'

CREDOMATIC_FILENAME_TEMPLATE = config['credomatic']['output_filename_template']
CREDOMATIC_FILENAME_REGEXP = config['credomatic']['source_file_regexp']

BCO_GENERAL_DATE_FORMAT = '%d/%m/%Y'
OUTPUT_FILENAME_TEMPLATE = 'gastos_%s.%s'

MAIN_SHEET_NAME = 'Gastos'

DEBIT_TYPE = 'DEBIT'
CREDIT_TYPE = 'CREDIT'


def print_constants(vars_dict):
    od = collections.OrderedDict(sorted(vars_dict.items()))
    for var, value in od.items():
        regexp = '^[A-Z][A-Z_]+'
        match = re.search(regexp, var)
        if match:
            print('%-35s : %r' % (var, value))


if __name__ == '__main__':
    print_constants(globals())