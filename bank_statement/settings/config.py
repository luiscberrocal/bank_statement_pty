import json

__author__ = 'lberrocal'
from os.path import dirname, abspath, exists, join
from os import getuid, environ
import logging
import configparser
import pwd
from platform import system

def write_default_config(config_filename=None):
    if config_filename is None:
        config_filename = 'default.json'
    if not exists(config_filename):
        my_configuration = {'user_data': {},
                            'credomatic': {}}
        print(system())
        if system() == 'Windows':
            username = environ.get('USERNAME')
            my_path = environ.get('USERPROFILE') + r'\Downloads'
        elif system() == 'Darwin':
            import pwd
            from os import getuid
            username = pwd.getpwuid(getuid())[0]
            my_path = '/Users/%s/Downloads/' % username
        else:
            username = 'Unkonwn'
            my_path = '/Users/%s/Downloads/' % username
        my_configuration['user_data']['username'] = username
        my_configuration['user_data']['data_path'] = my_path
        ## ---- Credomatic Section ----
        my_configuration['credomatic']['source_file_regexp'] = '5200570515342045\s\(\d*\)\.htm'
        my_configuration['credomatic']['output_filename_template'] = 'visa_credomatic_%s.htm'
        with open(config_filename,'w') as json_file:
            json.dump(my_configuration, json_file, indent=4)

def write_default_ini(ini_filename=None):
    if ini_filename is None:
        ini_filename = 'default.ini'
    if not exists(ini_filename):
        my_configuration = configparser.RawConfigParser()
        my_configuration.add_section('UserData')
        username = pwd.getpwuid( getuid() )[ 0 ]
        my_path = '/Users/%s/Downloads/' % username
        my_configuration.set('UserData', 'path', my_path)
        my_configuration.set('UserData', 'username', username)
        ## ---- Credomatic Section ----
        my_configuration.add_section('Credomatic')
        my_configuration.set('Credomatic', 'source_file_regexp', '5200570515342045\s\(\d*\)\.htm')
        my_configuration.set('Credomatic', 'output_filename_template', 'visa_credomatic_%s.htm')
        cfgfile = open(ini_filename,'w')
        my_configuration.write(cfgfile)
        cfgfile.close()


if __name__ == '__main__':
    write_default_config()