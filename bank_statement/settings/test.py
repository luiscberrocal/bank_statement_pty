__author__ = 'luiscberrocal'
from bank_statement.settings.base import *
import os

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

FOLDER_DATA = os.path.abspath(os.path.join(BANK_STATEMENT_ROOT, '..', 'test_data'))
OUTPUT_FOLDER = os.path.abspath(os.path.join(BANK_STATEMENT_ROOT, '..', 'output'))


if __name__ == '__main__':
    print('**** TEST ENVIRONMENT ****')
    print_constants(globals())