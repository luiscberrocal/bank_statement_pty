import csv
import os
from datetime import datetime

import openpyxl

from bank_statement.parsers import credomatic
from bank_statement.parsers.bco_general import parse_general_visa_html, parse_html_account
from bank_statement.settings.base import COLUMN_HEADER, FOLDER_DATA, OUTPUT_FILENAME_TEMPLATE, CREDOMATIC_FILENAME_TEMPLATE, \
    MAIN_SHEET_NAME

__author__ = 'luiscberrocal'


def write_csv(transactions, filename, header=None):
    with open(filename, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='|',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        if header:
            writer.writerow(header)
        writer.writerows(transactions)
    print('Filename %s written' % filename)

def write_excel(transactions, filename, header=None):
    if os.path.exists(filename):
        workbook = openpyxl.load_workbook(filename)
    else:
        workbook = openpyxl.Workbook()
        workbook.create_sheet(title=MAIN_SHEET_NAME)

    sheet = workbook.get_sheet_by_name(MAIN_SHEET_NAME)
    if header is not None:
        sheet.append(header)
    for transaction in transactions:
        sheet.append(transaction)
    workbook.save(filename)



def get_general_visa_transactions(output_filename):
    visa_filename_template = os.path.join(FOLDER_DATA, 'visa_clasica_%s_files/visa_mov.html')


    months = ['201401', '201402', '201403', '201404', '201405', '201406', '201407', '201408',
              '201409', '201410', '201411', '201412', '201501', '201502', '201503', '201504',
              '201505']
    count = 1
    for month in months:
        transactions = parse_general_visa_html(visa_filename_template % month, 'VISA Clasica')
        if count == 1:
            header = COLUMN_HEADER
        else:
            header = None
        write_excel(transactions, output_filename, header=header)
        count += 1


def write_checking_account_transactions(output_filename):
    account_filename_template = os.path.join(FOLDER_DATA, 'cuenta_corriente_%s_files/estados_cta.html')
    months = ['201401', '201402', '201403', '201404', '201405', '201406', '201407', '201408',
              '201409']
    for month in months:
        transactions = parse_html_account(account_filename_template % month, 'Cuenta Corriente')
        write_excel(transactions, output_filename, header=None)


def write_credomatic_transactions(output_filename):
    credomatic_visa_filename_template = os.path.join(FOLDER_DATA, CREDOMATIC_FILENAME_TEMPLATE)
    months = ['201401', '201402', '201403', '201404', '201405', '201406', '201407', '201408',
              '201409', '201411', '201412', '201501', '201502', '201503', '201504']
    for month in months:
        transactions = credomatic.parse_html_visa(credomatic_visa_filename_template % month, 'VISA Credomatic',
                                                  month[:-2])
        write_excel(transactions, output_filename, header=None)


if __name__ == "__main__":

    output_filename = os.path.join(FOLDER_DATA, OUTPUT_FILENAME_TEMPLATE %
                                   (datetime.now().strftime('%Y%m%d_%H%M'), 'xlsx'))
    get_general_visa_transactions(output_filename)

    write_checking_account_transactions(output_filename)

    write_credomatic_transactions(output_filename)

    print('=' * (len(output_filename) + 20) )
    print('%s file written' % output_filename)
    print('=' * (len(output_filename)  + 20) )





