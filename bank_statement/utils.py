import os
__author__ = 'luiscberrocal'

def split_path(filename):
    file_data = {'path': None,
                 'basename': None,
                 'extension': None,
                 'filename': None}
    data = os.path.split(filename)
    file_data['path'] = data[0]
    file_data['filename'] = data[1]
    filename_parts = data[1].split('.')

    file_data['basename'] = '.'.join(filename_parts[0:len(filename_parts)-1])
    file_data['extension'] = filename_parts[len(filename_parts)-1]
    return  file_data