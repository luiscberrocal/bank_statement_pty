import re
from datetime import datetime
import os
import bs4

from bank_statement.settings.base import BCO_GENERAL_DATE_FORMAT
from bank_statement.utils import split_path
import logging
logger = logging.getLogger(__name__)
__author__ = 'luiscberrocal'

def _convert_to_date(string_date):
    import locale
    locale.setlocale(locale.LC_ALL, 'es_ES')
    transaction_date = datetime.strptime(string_date, '%d/%b/%Y')
    return transaction_date.date()

def _get_account_type(account_number):
    """
    Returns the account type based on the account number format. Returns None if it doesn't match any
    of the account types

    Checking account: 03-22-01-099987-0
    Visa accont: 4530-10XX-XXXX-0000
    :param account_number: The number of the account
    """
    regexps= [{'regexp': r'^03-\d{2}-\d{2}-\d{6}-\d$', 'name': 'Checking account'},
              {'regexp': r'^4\d{3}-\d{2}XX-XXXX-\d{4}', 'name': 'VISA account'}]
    for regexp in regexps:
        match = re.match(regexp['regexp'], account_number)
        if match:
            return  regexp['name']
    return None


def _get_account_and_date_info(filename, encoding='latin-1'):
    data = {'account_number': None,
            'account_name': None,
            'account_type': None,
            'account_date': None}
    file_parts = split_path(filename)

    with open(filename, encoding=encoding) as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        if file_parts['basename'] == 'estados_cta':
            td_tag = html_soup.find('td', string='Cuenta nueva:')
            data['account_number'] = td_tag.parent()[1].text.strip()
            td_tag_name = html_soup.find('td', string='Nombre:')
            data['account_name'] = td_tag_name.parent()[1].text.strip()
            data['account_type'] = _get_account_type(data['account_number'])
        elif file_parts['basename'] == 'visa_mov':
            td_tag = html_soup.find('td', string='Número de	cuenta:')
            data['account_number'] = td_tag.parent()[1].text.strip()
            td_tag_name = html_soup.find('td', string='Nombre:')
            data['account_name'] = td_tag_name.parent()[1].text.strip()
            data['account_type'] = _get_account_type(data['account_number'])
            td_tag_date = html_soup.find('td', string='Fecha estado	de cuenta:')
            data['account_date'] = _convert_to_date(td_tag_date.parent()[1].text.strip())


    return data


def bank_data(filename, encoding='latin-1'):
    """
    When you download a statement from Banco General it downloadas an html file with the name yo
    gave it,lets say visa_gold.html and it also creates a folder named <your_filename>_files,
    for our example it would be visa_gold_files. Inside that folder is the html file with the
    actual data for your statement.

    This function extracts the information of the account and the location of the html file with the data
    """
    data = {'source_filename': None}
    with open(filename, encoding=encoding) as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        frame = html_soup.find('frame', {'name':'main'})
        source_file =frame.attrs['src']
        if source_file:
            file_parts = split_path(filename)
            regexp = r'^\./%s_files/(estados_cta|visa_mov)\.html$' % (file_parts['basename'])
            match = re.match(regexp, source_file)
            if match:
                data['source_filename'] = os.path.join(file_parts['path'], source_file[2:])
    data.update(_get_account_and_date_info(data['source_filename']))
    print(data)
    return data



def parse_html_account(html_filename, source):
    print('Parsing %s ...' % html_filename)
    date_regexp = r'(\d+/\d+/\d{4})'
    text_regexp = r'([\w]*)'
    description_regexp = r'([\w\s]*)'
    money_regexp=r'((\d+,)*(\d+\.\d{2}))'
    transactions = []
    with open(html_filename, encoding='latin-1') as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        rows = html_soup.findAll('tr')
        for row in rows:
            columns = row.findAll('td')
            if len(columns) == 6:
                match_transaction_date=re.search(date_regexp,columns[0].getText())
                if match_transaction_date:
                    transaction = []
                    transaction.append(datetime.strptime(match_transaction_date.group(1), BCO_GENERAL_DATE_FORMAT))
                    transaction.append(datetime.strptime(match_transaction_date.group(1), BCO_GENERAL_DATE_FORMAT))
                    transaction.append(columns[1].getText().replace(u'\xa0','').strip())
                    transaction.append(columns[2].getText().replace(u'\xa0','').strip())
                    money_match = re.search(money_regexp, columns[3].getText())
                    transaction_type = 'DEBIT'
                    if not money_match:
                        money_match = re.search(money_regexp, columns[4].getText())
                        transaction_type = 'CREDIT'
                    transaction.append(float(money_match.group(1).replace(',','')))
                    transaction.append(transaction_type)
                    transaction.append(source)
                    print(transaction)
                    transactions.append(transaction)
    return transactions


def parse_general_visa_html(html_filename, source):
    logger.debug('Parsing %s ...' % html_filename)
    date_regexp = r'(\d+/\d+/\d{4})'
    text_regexp = r'([\w]*)'
    description_regexp = r'([\w\s]*)'
    money_regexp=r'\$((\d+,)*(\d+\.\d{2}))'
    transactions = []
    with open(html_filename, encoding='latin-1') as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        rows = html_soup.findAll('tr')
        for row in rows:
            columns = row.findAll('td')
            if len(columns) == 6:
                match_transaction_date=re.search(date_regexp,columns[0].getText())
                match_process_date=re.search(date_regexp,columns[1].getText())
                if match_transaction_date and match_process_date:
                    logger.debug('Transaction date: %s' % match_transaction_date.group(1))
                    transaction = []
                    transaction.append(datetime.strptime(match_transaction_date.group(1), BCO_GENERAL_DATE_FORMAT))
                    transaction.append(datetime.strptime(match_process_date.group(1), BCO_GENERAL_DATE_FORMAT))
                    text_match = re.search(text_regexp,columns[2].getText())
                    transaction.append(text_match.group(1))
                    description_match = re.search(description_regexp,columns[3].getText())
                    transaction.append(columns[3].getText().replace(u'\xa0','').strip())
                    logger.debug('Money columns %s' % ( columns[4].getText()))
                    money_match = re.search(money_regexp, columns[4].getText())
                    transaction_type = 'DEBIT'
                    if not money_match:
                        money_match = re.search(money_regexp, columns[5].getText())
                        transaction_type = 'CREDIT'
                    transaction.append(float(money_match.group(1).replace(',','')))
                    transaction.append(transaction_type)
                    transaction.append(source)
                    print(transaction)
                    transactions.append(transaction)
    return transactions
