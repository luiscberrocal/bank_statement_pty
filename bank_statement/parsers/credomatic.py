import os
import re
from datetime import datetime
import logging

import bs4

from bank_statement.settings.base import FOLDER_DATA, OUTPUT_FILENAME_TEMPLATE, CREDOMATIC_FILENAME_TEMPLATE

logger = logging.getLogger(__name__)

__author__ = 'luiscberrocal'


def convert_date(string_date, year):
    date_regexp = r'((ENE|FEB|MAR|ABR|MAY|JUN|JUL|AGO|SEP|OCT|NOV|DIC)/\d{2})'
    date_match = re.search(date_regexp, string_date)
    transaction_date = None
    if date_match:
        import locale
        locale.setlocale(locale.LC_ALL, 'es_ES')
        actual_date = string_date.strip() + '/' + year
        transaction_date = datetime.strptime(actual_date, '%b/%d/%Y')
        #print(date_match.group(1), transaction_date.strftime('%d-%b-%Y'))
    return transaction_date.date()

def parse_html_visa_for_date(html_filename):
    #print('Parsing %s ...' % html_filename)
    text_regexp = r'Fecha\sde\sCorte\s*(\d+/\d+/\d{4})'
    statement_date = None
    with open(html_filename, encoding='latin-1') as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        rows = html_soup.findAll('tr')
        count = 1
        for row in rows:
            columns = row.findAll('td')
            match = re.search(text_regexp, columns[0].getText())
            if match:
                #print('-- %d %s' % (count, match.group(1)))
                statement_date = datetime.strptime(match.group(1), '%d/%m/%Y')
                break
    return statement_date

def parse_html_visa(html_filename, source, year):
    print('Parsing %s ...' % html_filename)
    date_regexp = r'(\d+/\d+/\d{4})'
    text_regexp = r'([\w]*)'
    description_regexp = r'([\w\s]*)'
    money_regexp=r'\$(((\d+,)*(\d+)?(\.\d{2}))([-]?))'
    transactions = []
    with open(html_filename, encoding='latin-1') as html_file:
        html_soup = bs4.BeautifulSoup(html_file.read(), 'html.parser')
        rows = html_soup.findAll('tr')
        for row in rows:
            columns = row.findAll('td')

            # for col in columns:
            #     print(col.getText())
            if len(columns) == 5:
                transaction_date = convert_date(columns[1].getText(), year)
                if transaction_date:
                    transaction = []
                    transaction.append(transaction_date)
                    transaction.append(transaction_date)
                    transaction.append(columns[0].getText().replace(u'\xa0','').strip())
                    transaction.append(columns[2].getText().replace(u'\xa0','').strip())
                    money_match = re.search(money_regexp, columns[4].getText())
                    #transaction.append(columns[4].getText().replace(u'\xa0','').strip())
                    transaction_type = 'DEBIT'
                    if money_match:
                        transaction.append(float(money_match.group(2).replace(',','')))
                        if money_match.group(6):
                            transaction_type = 'CREDIT'
                    transaction.append(transaction_type)
                    transaction.append(source)
                    #print(transaction)
                    logger.debug(transaction)
                    transactions.append(transaction)
    return transactions


if __name__ == '__main__':
    visa_filename_template = os.path.join(FOLDER_DATA,CREDOMATIC_FILENAME_TEMPLATE)


    output_filename = os.path.join(FOLDER_DATA, OUTPUT_FILENAME_TEMPLATE %
                                   (datetime.now().strftime('%Y%m%d_%H%M'), 'xlsx'))

    parse_html_visa(visa_filename_template % '201507', 'Visa Credomatic')