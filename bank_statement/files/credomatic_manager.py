import re
import shutil
from bank_statement.parsers.credomatic import parse_html_visa_for_date
from bank_statement.settings.base import CREDOMATIC_FILENAME_TEMPLATE, FOLDER_DATA, CREDOMATIC_FILENAME_REGEXP

__author__ = 'luiscberrocal'

import os


def rename_files(file_list):
    for file_data in file_list:
        source_file = os.path.join(file_data['source_folder'], file_data['filename'])
        target_file = os.path.join(file_data['target_folder'], file_data['new_filename'])
        shutil.copy(source_file, target_file)


def list_new_files(regexp, source_folder, target_folder=None):
    new_files = []
    if target_folder is None:
        target_folder = source_folder
    for filename in os.listdir(source_folder):
        match = re.match(regexp, filename)
        if match:
            statement_date = parse_html_visa_for_date(os.path.join(source_folder, filename))
            new_filename = CREDOMATIC_FILENAME_TEMPLATE % statement_date.strftime('%Y%m')
            new_files.append({'filename': filename,
                              'new_filename': new_filename,
                              'target_folder': target_folder,
                              'source_folder': source_folder})
    sorted_list = sorted(new_files, key=lambda k: k['new_filename'])
    return sorted_list

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('action', action="store")
    parser.add_argument('-s', '--source-directory', help='Directory containing html statements', action='store',
                        dest='source_directory', default=FOLDER_DATA)
    parser.add_argument('-t', '--target-directory', help='Directory to write the renamed files', action='store',
                        dest='target_directory')
    parser.add_argument('-r', '--regexp', help='Regular expression for source files', action='store',
                        dest='regexp', default=CREDOMATIC_FILENAME_REGEXP)
    results = parser.parse_args()

    if results.target_directory is None:
        target_directory = results.source_directory
    else:
        target_directory = results.target_directory

    new_files = list_new_files(results.regexp, results.source_directory, target_directory)
    if results.action == 'list':
        print('List Command')
        for filename in new_files:
            print(filename)
    elif results.action == 'copy':
        print('Copy Command')
        rename_files(new_files)

    print('Found %d with format %s files in %s' % (len(new_files), results.regexp,  results.source_directory))


if __name__ == '__main__':
    main()

