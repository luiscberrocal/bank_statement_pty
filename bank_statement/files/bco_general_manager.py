from os.path import basename
import re
from bank_statement.settings.base import FOLDER_DATA

__author__ = 'luiscberrocal'
import os

def list_files_to_process(source_directory):
    for dirName, subdirList, fileList in os.walk(source_directory):
        folder = basename(dirName)
        match = re.match(r'(visa_clasica|cuenta_corriente)_\d{6}_files', folder)
        if match:
            print('Found directory: %s' % dirName)
            for fname in fileList:
                if fname == 'visa_mov.html' or fname == 'estados_cta.html':
                    print('\t%s' % fname)


if __name__ == '__main__':
    list_files_to_process(FOLDER_DATA)