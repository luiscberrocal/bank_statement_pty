import re

__author__ = 'luiscberrocal'
import openpyxl
from bank_statement.settings.base import FOLDER_DATA, MAIN_SHEET_NAME, DEBIT_TYPE
import os

CLASSIFIER_RULES = [['GASTOS BANCARIOS', ['MEMBRESÍA\sANUAL\sDIFERIDA',
                                          'SERV\sALERTAS\sDE',
                                          'PROTECCION\sROBO',
                                          'ITBMS\sCARGO\sPOR\sSEGURO',
                                          'SEGURO\sDE\sDESGRAVAMEN',
                                          'SEGURO\sFRAUDE\sMENSUAL',
                                          'MEMBRESIA\sANUAL',
                                          'SUBSIDIO,\sLEY\s4',
                                          'PLAN\sSALDOS\sDEU',
                                          'SALDO\sDEUDOR\sPM',
                                          'ITBMS\sCARGO\sDE\sMEMBRESIA']],
                    ['CASH', ['ATM-BANCO\s']],
                    ['SUPERMERCADO', ['REY\s', 'SUPER\s99\s', 'RIBA\sSMITH\s']],
                    ['CINE', ['CINEPOLIS\s', 'CINEMARK\s', 'CINEFOOD\s', 'CINE\sFOOD']],
                    ['COMBUSTIBLE', ['ESTACION\sPUMA\s', 'PUMA\sENERGY\s', 'ESTACION\sTALLER\sLA\sBOC',
                                     'DELTA\sHATO\sPINTADO', 'DELTA-\sVISTA\sHERMOSA', 'ESSO\s']],
                    ['RESTAURANTE',['RESTAURANTE\s','POPEYES', 'LA\sCARRETA', 'ATHENS\sPIZZA',
                                    'T\.G\.I\.[\s]?FRIDAY', 'POLLO\sTROPICAL', 'REST\.\sEL\sMESON\sDEL\sPRA',
                                    'SUBWAY\s', 'SUSHI\sEXPRESS', 'REST\.\sESTILO\sCAMPO', 'NIKO\sS\sCAFE',
                                    'LE\?OS\s[&Y]\sCARBON',]],
                    ['ARROCHA',['ARROCHA[\s-]']],
                    ['CABLE INTERNET', ['NETFLIX.COM', 'PAYPAL\s\*LONDONTRUST', 'CABLE\sONDA\sRECURRENTES']],
                    ['PRICESMART', ['PRICESMART\s']],
                    ['COMIDA CHINA', ['HUNGS\sCENTER']],
                    ['CELULAR', ['CLARO\sPANAMA']],
                    ['HELADO DULCE', ['THE\sYOGURT\sFACTORY', 'CINABON', 'CINNABON']],
                    ['INTERESES', ['FINANCIAMIENTO','INTERESES\sCORRIENTES\sDOLARES', 'RECARGO\sPOR\sPAGO\sATRASADO']],
                    ['FERRETERIA', ['NOVEY','DO\sIT\sCENTER']],
                    ['RENTA', ['BANCA\sEN\sLINEA\s-\sTRANSFERENCIA\sA\s0401017952987']],
                    ['AUTO',['MULTIBANK-10343369053']]
                    ]
def gastos_bancarios(description):
    for rule in CLASSIFIER_RULES:
        if len(rule[1]) == 1:
            regexp = r'(%s)' % rule[1][0]
        else:
            regexp = r'(%s)' % '|'.join(rule[1])

        #print(regexp)
        match = re.search(regexp, description)
        if match:
            return rule[0]
    return ''

if __name__ == '__main__':
    filename = os.path.join(FOLDER_DATA, 'gastos_20150823_1410.xlsx')
    output_filename = os.path.join(FOLDER_DATA, 'gastos_20150823_1410-2.xlsx')

    workbook = openpyxl.load_workbook(filename)
    sheet = workbook.get_sheet_by_name(MAIN_SHEET_NAME)
    more_data = True
    row_num = 2
    classified = 0
    debit_count = 0
    while more_data:
        if sheet.cell(row=row_num, column=6).value == DEBIT_TYPE:
            debit_count += 1
            description = sheet.cell(row=row_num, column=4).value

            expense_type = gastos_bancarios(description)
            if len(expense_type) != 0:
                classified += 1
            if len(expense_type) == 0:
                print('%4d -- %-50s %s' % (row_num, description, expense_type))
            else:
                sheet.cell(row=row_num, column=8).value = expense_type
        if sheet.cell(row=row_num, column=4).value is None:
            break
        row_num += 1

    print('Debits %d Classified %d' %(debit_count, classified))
    workbook.save(output_filename)
