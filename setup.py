__author__ = 'luiscberrocal'
from setuptools import setup, find_packages

setup(
    name='bank-statement',
    version='0.1.5',
    description='Tool to help extract data from html downloads of bank statements',
    author='Luis Carlos Berrocal',
    author_email ='luis.berrocal.1942@gmail.com',
    packages=find_packages(),
    url='https://bitbucket.org/luiscberrocal/bank_statement_pty',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.4',
    ],
    install_requires=[
        'beautifulsoup4==4.4.0',
        'openpyxl==2.2.5',
        'appdirs==1.4.0',
        'termcolor==1.1.0'
    ],
    entry_points = {
        'console_scripts': ['credomatic-manager=bank_statement.files.credomatic_manager:main',
                            'bank-statement-config=bank_statement.settings.base:print_constants'],
    }
)